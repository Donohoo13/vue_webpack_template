import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './views/App.vue';
import routes from './utilities/Router.js';

Vue.use(VueRouter);
const router = new VueRouter({ routes });

new Vue({
	router,
	render: h => h(App)
}).$mount('#app');
