import Home from '../components/Home.vue';
const routes = [
	{
		path: '/',
		name: 'home',
		component: Home
	}
];

export default routes;
